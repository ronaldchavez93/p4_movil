import { Component, OnInit } from '@angular/core';
import {  FormBuilder,
          FormGroup,
          Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  formLogin: FormGroup;
  btnLogin: boolean;

  constructor(  private fb: FormBuilder,
                private router: Router,
                private _loginService: LoginService) { }

  ngOnInit() {
    this.buildValidations();
  }

  buildValidations(): void {

    this.formLogin = this.fb.group({
      email: ['', [ Validators.required,
                    Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'), 
                    Validators.minLength(10),
                    Validators.maxLength(50)
      ]],
      password: ['', [ Validators.required, 
        Validators.minLength(4),
        Validators.maxLength(15)
        ]]
    });

  }

  async login(){

    this.btnLogin = true;
    let frm = this.formLogin.value;

    try{
      let data: any = await this._loginService.login(frm).toPromise();
      localStorage.setItem('_access_token_', data.sessionTokenBck);
      this.router.navigate(['/dashboard']);
      this.formLogin.reset();
      this.btnLogin = false;
    } catch( err ){
      console.log(err);
      this.btnLogin = false;
    }
  }



}
