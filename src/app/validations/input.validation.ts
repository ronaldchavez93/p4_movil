import { Injectable } from '@angular/core';

@Injectable()
export class Validation {

	onlyNumbers(key: any){

		let tecla = String.fromCharCode(key).toLowerCase();
		let number = "0123456789";

		if(number.indexOf(tecla) == -1 && key != 8)
			return false;

	}

    onlyPrices(key: any){

		let tecla = String.fromCharCode(key).toLowerCase();
		let number = "0123456789,";

		if(number.indexOf(tecla) == -1 && key != 8)
			return false;

	}
}