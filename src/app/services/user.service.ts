import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  customers(token: string){
    const httpOptionsToken = { headers: new HttpHeaders({   'Accept': 'application/json',
                                                            adminemail: 'testapis@tuten.cl',
                                                            app: 'APP_BCK',
                                                            token: token })};
    return this.http.get(`https://dev.tuten.cl/TutenREST/rest/user/contacto@tuten.cl/bookings?current=true`,httpOptionsToken);
  }

}
