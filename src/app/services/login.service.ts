import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Observable } from 'rxjs'

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) {  }

  login(data: any): Observable<Object>{
    const httpOptionsToken = { headers: new HttpHeaders({   'Accept': 'application/json',
                                                            password: data.password,
                                                            app: 'APP_BCK' })};
    return this.http.put<Object>(`https://dev.tuten.cl/TutenREST/rest/user/${data.email}`,{},httpOptionsToken);
  }

}
