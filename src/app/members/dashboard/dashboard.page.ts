import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Validation } from 'src/app/validations/input.validation';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  token = localStorage.getItem('_access_token_') || '';
  customers: any[] = [];
  listCustomers: any[] = [];
  filter: string = 'BookingId';
  priceMin: string = '';
  priceMax: string = '';
  loading: boolean;

  constructor(private router: Router,
              private _userService: UserService,
              private _validation: Validation) { }

  ngOnInit() {
    this.getCustomers();
  }

  async getCustomers(){

    this.loading = true;

    try{

      let data: any = await this._userService.customers(this.token).toPromise();
      this.customers = data;
      this.listCustomers = data;
      this.loading = false;

    } catch( err ){
      console.log(err);
      this.loading = false;
    }
  }

  searchCustomerBookingId( text: string ){

    if(text.length > 0){
      let query = text;
      this.customers = this.listCustomers.filter( ( data: any ) => 
                                                                  data.bookingId.toString().indexOf(query) > -1 );
    }else{
      this.customers = this.listCustomers;
    }

  }

  selectFilter(value: string){
    this.filter = value;
  }

  searchCustomerbookingPrice(){

    let queryMin = this.priceMin.replace(",", "");
    let queryMax = this.priceMax.replace(",", "");
    if(queryMin.length > 0 && queryMax.length > 0){

        if(queryMin > queryMax){
            return;
        }

        this.customers = this.listCustomers.filter(  data => 
                                                            (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice)  >=  queryMin && 
                                                            (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice) <=  queryMax );
    }else if(queryMin.length > 0){

        this.customers = this.listCustomers.filter(  data => 
                                                            (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice)  >=  queryMin );
    }else if(queryMax.length > 0){
      this.customers = this.listCustomers.filter(  data => 
                                                            (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice) <=  queryMax );
    }else{
      this.customers = this.listCustomers;
    }
  }

  logout(){
      localStorage.removeItem('_access_token_');
      this.router.navigate(['/']);
  }

}
