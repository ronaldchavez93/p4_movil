import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { DashboardPage } from './dashboard.page';

/* SERVICIOS */
import {UserService} from 'src/app/services/user.service';
import {Validation} from 'src/app/validations/input.validation';
/* FIN */

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule
  ],
  declarations: [DashboardPage],
  providers: [UserService, Validation]
})
export class DashboardPageModule {}
